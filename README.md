# weather-server

#### 介绍
天气预报API

#### 软件架构
PHP+腾讯地图API


#### 使用说明
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
location_type | 2 | Integer | 是 | 地址类型；0：ip定位；1：经纬度定位；2：省市县
lng | 119.645957 | String | 否 | 经度
lat | 28.007508 | String | 否 | 纬度
province | 浙江省 | String | 否 | 省
city | 宁波市 | String | 否 | 市
area | 慈溪市 | String | 否 | 县                                           |

#### 参与贡献

1.  代码来源[vue-mini-weather](https://github.com/hjiachuang/vue-mini-weather)

